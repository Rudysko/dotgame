import java.util.*;
public class DotComBust{
  private GameHelper helper = new GameHelper();
  private int numOfGuesses = 0;
  private ArrayList<DotCom> dotComsList = new ArrayList<DotCom>();
  private void setUpGame(){

    DotCom dot = new DotCom();
    DotCom dot1 = new DotCom();
    DotCom dot2 = new DotCom();
    dot.setName("google.com");
    dot1.setName("yahoo.com");
    dot2.setName("wp.pl");
    dotComsList.add(dot);
    dotComsList.add(dot1);
    dotComsList.add(dot2);
        for( DotCom dotComToSet : dotComsList){

        ArrayList<String> newLocation = helper.placeDotCom(3);
        dotComToSet.setLocationCells(newLocation);
        }

  }
    public void startPlaying(){
      while(!dotComsList.isEmpty()){

        String userGuess =   helper.getUserInput("Zgaduj");
        checkUserGuess(userGuess);
      }
      finishGame();
    }
      private void checkUserGuess(String userGuess){
          numOfGuesses++;
          String result = "miss";
            for ( DotCom dotComToTest : dotComsList){
              result = dotComToTest.checkYourself(userGuess);
                if(result.equals("hit")){
                  break;
                }
                if (result.equals("kill")){
                  dotComsList.remove(dotComToTest);
                  break;
                }

            }
            System.out.println(result);
      }

        public void finishGame(){
          System.out.println("Zabiles wszystko co sie da");
        }

        public static void main(String[] args){

          DotComBust game = new DotComBust();
          game.setUpGame();
          game.startPlaying();

        }





}
